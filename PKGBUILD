# Maintainer: Philip Müller <philm[at]manjaro[dot]org>
# Maintainer: Bernhard Landauer <bernhard[at]manjaro[dot]org>
# Maintainer: Helmut Stult <helmut[at]manjaro[dot]org>

_linuxprefix=linux-xanmod-lts
_extramodules=extramodules-6.1-xanmod
pkgname=("$_linuxprefix-zfs")
_pkgver=2.1.9
pkgver=2.1.9_6.1.23.xanmod1_1
pkgrel=1
url="http://zfsonlinux.org/"
arch=('x86_64')
license=("CDDL")
depends=("kmod")
makedepends=("$_linuxprefix" "$_linuxprefix-headers")
groups=("$_linuxprefix-extramodules")
install=install
source=("https://github.com/zfsonlinux/zfs/releases/download/zfs-${_pkgver}/zfs-${_pkgver}.tar.gz")
sha256sums=('6b172cdf2eb54e17fcd68f900fab33c1430c5c59848fa46fab83614922fe50f6')

pkgver() {
    _ver=$(pacman -Q $_linuxprefix | cut -d " " -f 2 | cut -f1-2 -d'-')
    printf '%s' "${_pkgver}_${_ver/-/_}"
}

build() {
  _kernver="$(cat /usr/lib/modules/${_extramodules}/version)"
  cd "${srcdir}/zfs-${_pkgver}"
  ./autogen.sh
  sed -i "s|\$(uname -r)|${_kernver}|g" configure
  ./configure --prefix=/usr --sysconfdir=/etc --sbindir=/usr/bin --libdir=/usr/lib \
              --datadir=/usr/share --includedir=/usr/include --with-udevdir=/lib/udev \
              --libexecdir=/usr/lib/zfs-${_pkgver} --with-config=kernel \
              --with-linux=/usr/lib/modules/${_kernver}/build \
              --with-linux-obj=/usr/lib/modules/${_kernver}/build
  make
}

package(){
  _ver=$(pacman -Q $_linuxprefix | cut -d " " -f 2)
  pkgdesc='Kernel modules for the Zettabyte File System.'
  provides=("zfs=$_pkgver")
  depends+=("${_linuxprefix}=${_ver}" "zfs-utils=${_pkgver}")
  replaces=('linux510-xanmod-zfs')

  cd "${srcdir}/zfs-${_pkgver}"
  install -dm755 "$pkgdir/usr/lib/modules/$_extramodules"
  install -m644 module/*/*.ko "$pkgdir/usr/lib/modules/$_extramodules"
  find "$pkgdir" -name '*.ko' -exec gzip -9 {} +
  sed -i -e "s/EXTRAMODULES='.*'/EXTRAMODULES='$_extramodules'/" "$startdir/install"
}
